import React from 'react';
import './App.css';
import { Route } from "react-router-dom";

import Hello from "./pages/hello";
import Home from "./pages/home";
import Contact from "./pages/contact";

const routes = [
  { path: '/', name: 'Hello', Component: Hello },
  { path: '/home', name: 'Home', Component: Home },
  { path: '/contact', name: 'Contact', Component: Contact }
];

function App() {

  return (
    <div className='App'>
      {routes.map(({ path, Component }) => (
        <Route key={path} path={path} exact>
          <div className="page">
            <Component/>
          </div>
        </Route>
      ))}
    </div>
  );
}

export default App;
