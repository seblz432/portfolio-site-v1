import React, { useEffect, useRef } from 'react';
import { useHistory } from "react-router-dom";

import Button from '@material-ui/core/Button';

import { gsap } from 'gsap';
import ReactScrollWheelHandler from "react-scroll-wheel-handler";

import './contact.css';

const Contact = () => {

  const history = useHistory();

  let menuButtons = useRef(null);
  let line1 = useRef(null);
  let line2 = useRef(null);
  let line3 = useRef(null);
  let line4 = useRef(null);
  let line5 = useRef(null);
  let helloPage = useRef(null);
  let content = useRef(null);

  let canSwipe = "";

  if (window.innerWidth < 600) {
    canSwipe = false;
  } else {
    canSwipe = true;
  }

  useEffect(() => {
    /* ON LOAD */
    gsap.from([helloPage], {
      duration: 0.8,
      ease: "power1.in",
      x: '100%',
    });
    gsap.from([menuButtons, line1, line2, line3, line4, line5], {
      delay: 0.6,
      ease: "power1.out",
      y: '107%',
      scaleX: '0.9',
      stagger: {
        amount: 0.5
      }
    });
    gsap.from([menuButtons, content], {
      duration: 1.4,
      ease: "power1.out",
      x: '100vw',
    });

    /* ON LEAVE */
    return () => {
      console.log("exit");
    }
  }, [menuButtons, line1, line2]);

  const handleExit = (path) => {
    if (history.location.pathname === "/contact") {
      localStorage.setItem('exitPage', '/contact');

      gsap.to([menuButtons, content], {
        duration: 0.75,
        ease: "power1.in",
        x: '100vw',
      });
      gsap.to([menuButtons, line1, line2, line3, line4, line5], {
        duration: 0.4,
        ease: "power3.in",
        y: '107%',
        scaleX: '0.9',
        stagger: {
          amount: 0.1
        }
      });
      gsap.to([helloPage], {
        duration: 0.6,
        delay: 0.4,
        ease: "power1.in",
        x: '100%',
      });

      setTimeout(function() {
        history.push(path);
      }, 1175)
    }
  }

  return (
    <ReactScrollWheelHandler
      rightHandler={function(){handleExit("/home")}}

      pauseListeners={canSwipe}
      timeout={800}
    >
      <div id="menu">
        <div className="divider"/>

        <div className="menuLine">
          <div ref={el => (menuButtons = el)}>
            <Button
              className="menuButton"
              variant="outlined"
              onClick={function(){handleExit("/")}}
            >
              Greetings
            </Button>

            <Button
              className="menuButton"
              variant="outlined"
              onClick={function(){handleExit("/home")}}
            >
              About Me
            </Button>

            <Button
              className="menuButton"
              variant="outlined"
              id="currentPage"
              disabled
            >
              Contact
            </Button>
          </div>
        </div>
      </div>

      <div id="contactPage" ref={el => (helloPage = el)}>
        <div id="contactContent" ref={el => (content = el)}>
          <br/>

          <div id="mainSection">
            <div className="contactTextLine">
              <h1 className="contactH3" ref={el => (line1 = el)}>Let's get in touch!</h1>
            </div>

            <div className="contactTextLine">
              <h1 className="contactH3" ref={el => (line2 = el)}>
                <a href="mailto:dev@sebastiangale.ca">
                  <u>
                    dev@sebastiangale.ca
                  </u>
                </a>
              </h1>
            </div>

            <div className="contactDivider"/>

            <div className="contactH2Line">
              <h1 className="contactH2" ref={el => (line3 = el)}>
                <a href="https://gitlab.com/seblz432" target="_blank" rel="noopener noreferrer">
                  <u>
                    https://gitlab.com/seblz432
                  </u>
                </a>
              </h1>
            </div>

            <div className="contactH2Line">
              <h1 className="contactH2" ref={el => (line4 = el)}>
                LinkedIn:&nbsp;
                <a href="https://www.linkedin.com/in/sebastiangale" target="_blank" rel="noopener noreferrer">
                  <u>
                    linkedin.com/in/sebastiangale
                  </u>
                </a>
              </h1>
            </div>

            <div className="contactH2Line">
              <h1 className="contactH2" ref={el => (line5 = el)}>
                GitHub:&nbsp;
                <a href="https://github.com/seblz432" target="_blank" rel="noopener noreferrer">
                  <u>
                    https://github.com/seblz432
                  </u>
                </a>
              </h1>
            </div>

          </div>

          {/*<img id="contactImage" src={cartoonPortrait} alt=""/>*/}
          </div>
      </div>
    </ReactScrollWheelHandler>
  )
}

export default Contact;
