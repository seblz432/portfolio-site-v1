import React, { useEffect, useRef } from 'react';
import { useHistory } from "react-router-dom";

import Button from '@material-ui/core/Button';

import { gsap } from 'gsap';
import ReactScrollWheelHandler from "react-scroll-wheel-handler";

import './home.css';

import cartoonPortrait from '../portrait400.png';
import VEQImage from '../VEQlogo.png';
import BECOImage from '../BECO.png';
import GarrettShannon from '../garrettShannonLogo.png';

const Home = () => {

  const history = useHistory();

  let name = useRef(null);
  let title2 = useRef(null);
  let homePage = useRef(null);

  let textLine1 = useRef(null);
  let textLine2 = useRef(null);
  let textLine3 = useRef(null);
  let textLine4 = useRef(null);
  let textLine5 = useRef(null);
  let textLine6 = useRef(null);
  let textLine7 = useRef(null);

  let veqLink = useRef(null);
  let veqImage = useRef(null);
  let veqDescription = useRef(null);

  let portfolioLink = useRef(null);
  let portfolioImage = useRef(null);
  let portfolioDescription = useRef(null);

  let becoLink = useRef(null);
  let becoImage = useRef(null);
  let becoDescription = useRef(null);

  let menuButton = useRef(null);

  let canSwipe = "";

  if (window.innerWidth < 600) {
    canSwipe = false;
  } else {
    canSwipe = true;
  }

  useEffect(() => {
    /* ON LOAD */
    let enterTranslate;

    if (localStorage.getItem('exitPage') === '/contact') {
      enterTranslate = '-100%';
    } else {
      enterTranslate = '100%';
    }

    gsap.from([homePage], {
      duration: 0.8,
      ease: "power1.out",
      translateX: enterTranslate,
    });
    gsap.from([menuButton, name, textLine1, textLine2, textLine3, textLine4, textLine5, textLine6, textLine7, title2, veqDescription, veqLink, portfolioDescription, portfolioLink, becoDescription, becoLink], {
      ease: "power1.out",
      y: '107%',
      scaleX: '0.9',
      stagger: {
        amount: 0.8
      }
    });
    gsap.from([veqImage, portfolioImage, becoImage], {
      delay: 0.5,
      duration: 1,
      ease: "power1.in",
      opacity: 0,
      stagger: {
        amount: 0.5
      }
    });

    /* ON LEAVE */
    return () => {
      console.log("exit");
    }
  }, [name]);

  const handleExit = (path) => {
    if (history.location.pathname === "/home") {
      localStorage.setItem('exitPage', '/home');

      gsap.to([menuButton, name, textLine1, textLine2, textLine3, textLine4, textLine5, textLine6, textLine7, title2, veqDescription, veqLink, portfolioDescription, portfolioLink, becoDescription, becoLink], {
        ease: "power1.in",
        y: '107%',
        scaleX: '0.9',
        stagger: {
          amount: 0.15
        }
      });

      let exitTranslation;

      if (path === "/contact") {
        exitTranslation = '-100%';
      } else {
        exitTranslation = '100%';
      }

      gsap.to([homePage], {
        duration: 0.8,
        ease: "power1.in",
        translateX: exitTranslation,
      });

      setTimeout(function() {
        history.push(path);
      }, 800)
    }
  }

  return (
    <div id="homePage" ref={el => (homePage = el)}>
      <ReactScrollWheelHandler
        rightHandler={function(){handleExit("/")}}
        leftHandler={function(){handleExit("/contact")}}

        pauseListeners={canSwipe}
        timeout={800}
      >
        <div id="menu">
          <div className="divider"/>

          <div className="menuLine">
            <div ref={el => (menuButton = el)}>
              <Button
                className="menuButton"
                variant="outlined"
                onClick={function(){handleExit("/")}}
              >
                Greetings
              </Button>b

              <Button
                className="menuButton"
                id="currentPage"
                variant="outlined"
                disabled
              >
                About Me
              </Button>

              <Button
                className="menuButton"
                variant="outlined"
                onClick={function(){handleExit("/contact")}}
              >
                Contact
              </Button>
            </div>
          </div>
        </div>

        <div id="homeContent">
          <br/>
          <div id="homeMainSection">
            <div id="homeNameLine">
              <h2 className="homeName" ref={el => (name = el)}>
                Who the h*ll am I?
              </h2>
            </div>

            <div className="homeTextLine">
              <h3 className="homeH3" ref={el => (textLine1 = el)}>
                My name is Sebastian; I am a proud
              </h3>
            </div>
            <div className="homeTextLine">
              <h3 className="homeH3" ref={el => (textLine2 = el)}>
                Canadian web developer & designer
              </h3>
            </div>
            <div className="homeTextLine">
              <h3 className="homeH3" ref={el => (textLine3 = el)}>
                with a passion for technology. I like
              </h3>
            </div>
            <div className="homeTextLine">
              <h3 className="homeH3" ref={el => (textLine4 = el)}>
                doing work I can be proud of with great
              </h3>
            </div>
            <div className="homeTextLine">
              <h3 className="homeH3" ref={el => (textLine5 = el)}>
                attention to detail. Most importantly, I
              </h3>
            </div>
            <div className="homeTextLine">
              <h3 className="homeH3" ref={el => (textLine6 = el)} id="homeLastLine">
                will help you realize your dream website
              </h3>
            </div>
            <div className="homeTextLine">
              <h3 className="homeH3" ref={el => (textLine7 = el)} id="homeLastLine">
                from the design to code.
              </h3>
            </div>

            <div className="divider"/>

            <div id="homeH1Line">
              <h2 className="homeName" ref={el => (title2 = el)}>
                Past Work
              </h2>
            </div>

            {/* PAST WORK */}
              <div className="projectTextLine">
                <h2 className="projectDescriptionText" style={{color: '#7AAABF'}} ref={el => (veqDescription = el)}>
                  Video Portfolio for a Cinematographer
                </h2>
              </div>

              <center>
                <div className="hoverGrow" id="VEQImage" style={{backgroundColor: '#141414'}}>
                  <a href="https://garrettshannon.com/" target="_blank" rel="noopener noreferrer">
                    <img id="VEQlogo" alt="GarrettShannon" src={GarrettShannon} ref={el => (veqImage = el)}/>
                  </a>
                  </div>
              </center>

              <div className="homeTextLine projectLink">
                <h3 className="homeH3" ref={el => (veqLink = el)}>
                  <u>
                    <a href="https://garrettshannon.com/" target="_blank" rel="noopener noreferrer">
                      https://garrettshannon.com/
                    </a>
                  </u>
                </h3>
              </div>

              <div className="divider"/><div className="divider"/>

              <div className="projectTextLine">
                <h2 className="projectDescriptionText" style={{color: '#b54672'}} ref={el => (veqDescription = el)}>
                  Website for a Belgian Health Center
                </h2>
              </div>

              <center>
                <div className="hoverGrow" id="VEQImage" style={{backgroundColor: '#d1d1d1'}}>
                  <a href="https://voyageenquintessence.be" target="_blank" rel="noopener noreferrer">
                    <img id="VEQlogo" alt="Voyage en Quintessence" src={VEQImage} ref={el => (veqImage = el)}/>
                  </a>
                  </div>
              </center>

              <div className="homeTextLine projectLink">
                <h3 className="homeH3" ref={el => (veqLink = el)}>
                  <u>
                    <a href="https://voyageenquintessence.be" target="_blank" rel="noopener noreferrer">
                      https://voyageenquintessence.be
                    </a>
                  </u>rel="noreferrer"
                </h3>
              </div>

              <div className="divider"/><div className="divider"/>

              <div className="projectTextLine">
                <h2 className="projectDescriptionText" style={{color: '#c5b3ab'}} ref={el => (portfolioDescription = el)}>
                  My Personal Portfolio Website
                </h2>
              </div>

              <center>
                <div className="hoverGrow" id="VEQImage" style={{backgroundColor: '#512525'}}>
                  <a href="https://www.sebastiangale.ca">
                    <img id="VEQlogo" alt="Sebastian Gale Cartoon Portrait" src={cartoonPortrait} ref={el => (portfolioImage = el)}/>
                  </a>
                </div>
              </center>

              <div className="homeTextLine projectLink">
                <h3 className="homeH3" ref={el => (portfolioLink = el)}>
                  <u>
                    <a href="https://sebastiangale.ca">
                      https://www.sebastiangale.ca
                    </a>
                  </u>
                </h3>
              </div>

              <div className="divider"/><div className="divider"/>

              <div className="projectTextLine">
                <h2 className="projectDescriptionText" style={{color: '#4f693d'}} ref={el => (becoDescription = el)}>
                  An Online Eco-Friendly Store
                </h2>
              </div>

              <center>
                <div className="hoverGrow" id="VEQImage" style={{backgroundColor: '#1c210c'}}>
                  <a href="https://beecofriendly.ca/" target="_blank" rel="noopener noreferrer">
                    <img id="VEQlogo" alt="Beecofriendly" src={BECOImage} ref={el => (becoImage = el)}/>
                  </a>
                </div>
              </center>

              <div className="homeTextLine projectLink">
                <h3 className="homeH3" ref={el => (becoLink = el)}>
                  <u>
                    <a href="https://beecofriendly.ca/" target="_blank" rel="noopener noreferrer">
                      https://beecofriendly.ca/
                    </a>
                  </u>
                </h3>
              </div>

            {/* END OF PAST WORK */}
          </div>
          <div className="divider"/>
        </div>
      </ReactScrollWheelHandler>
    </div>
  )
}

export default Home;
