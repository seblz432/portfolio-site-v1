import React, { useEffect, useRef } from 'react';
import { useHistory } from "react-router-dom";

import Button from '@material-ui/core/Button';
import TouchAppIcon from '@material-ui/icons/TouchApp';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';

import { gsap } from 'gsap';
import ReactScrollWheelHandler from "react-scroll-wheel-handler";

import './hello.css';
import cartoonPortrait from '../portrait.png';

const Hello = () => {
  const history = useHistory();

  /*const [width, setWidth] = useState(window.innerWidth);
  const [height, setHeight] = useState(window.innerHeight);

  const updateWidthAndHeight = () => {
    setWidth(window.innerWidth);
    setHeight(window.innerHeight);
  };

  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);

  useEffect(() => {
    window.addEventListener("resize", updateWidthAndHeight);
    return () => window.removeEventListener("resize", updateWidthAndHeight);
  });*/

  let canSwipe = (window.innerWidth < 600) ? false : true;

  let name = useRef(null);
  let name1 = useRef(null);
  let line1 = useRef(null);
  let line2 = useRef(null);
  let helloPage = useRef(null);
  let content = useRef(null);
  let image = useRef(null);
  let menuButton = useRef(null);

  useEffect(() => {

    gsap.from([helloPage], {
      duration: 0.8,
      ease: "power1.in",
      x: '-100%',
    });
    gsap.from([menuButton, name, name1, line1, line2], {
      delay: 0.6,
      ease: "power1.out",
      y: '107%',
      scaleX: '0.9',
      stagger: {
        amount: 0.5
      }
    });
    gsap.from([content, menuButton, image], {
      delay: 0.2,
      duration: 1.4,
      ease: "power1.out",
      x: '-100vw',
    });
  }, [name, line1, line2]);

  const handleExit = (path) => {
    if (history.location.pathname === "/") {
      localStorage.setItem('exitPage', '/');

      gsap.to([content, menuButton, image], {
        duration: 0.75,
        ease: "power1.in",
        x: '-100vw',
      });
      gsap.to([line2, line1, name1, name, menuButton], {
        duration: 0.4,
        ease: "power3.in",
        y: '107%',
        scaleX: '0.9',
        stagger: {
          amount: 0.225
        }
      });
      gsap.to([helloPage], {
        duration: 0.6,
        delay: 0.55,
        ease: "power1.in",
        x: '-100%',
      });

      setTimeout(function() {
        history.push(path);
      }, 1175)
    }
  }

  return (
    <ReactScrollWheelHandler
      leftHandler={function(){handleExit("/home")}}

      pauseListeners={canSwipe}
      timeout={800}
    >
      <div id="menu">
        <div className="divider"/>

        <div className="menuLine">
          <div ref={el => (menuButton = el)}>
            <Button
              className="menuButton"
              variant="outlined"
              id="currentPage"
              disabled
            >
              Greetings
            </Button>

            <Button
              className="menuButton"
              variant="outlined"
              onClick={function(){handleExit("/home")}}
            >
              About Me
            </Button>

            <Button
              className="menuButton"
              variant="outlined"
              onClick={function(){handleExit("/contact")}}
            >
              Contact
            </Button>
          </div>
        </div>
      </div>

      <div id="helloPage" ref={el => (helloPage = el)}>
        <div id="content" ref={el => (content = el)}>

          <div className="helloSpacer"/>

          <div class="nameLine">
            <h2 ref={el => (name = el)}>
              Hello there!
            </h2>
          </div>

          <div class="nameLine">
            <h2 ref={el => (name1 = el)}>
              I'm Sebastian.
            </h2>
          </div>

          <div className="helloSpacer"/>

          <div className="titleLine">
            <h1 className="helloH1" ref={el => (line1 = el)}>Web developer & designer</h1>
          </div>
          <div className="titleLine">
            <h1 className="helloH1" ref={el => (line2 = el)}>at your service.</h1>
          </div>

          <div className="helloSpacer"/>

          <div className="moveLeft">
            <KeyboardArrowLeftIcon className="helloIcons" style={{marginBottom: '34px', marginLeft: '-3.7vw'}}/>
            <TouchAppIcon className="helloIcons" style={{marginBottom: '28px', marginLeft: '-25px'}}/>
          </div>

          <div id="imageDiv" ref={el => (image = el)}>
            <img id="helloPortrait" src={cartoonPortrait} alt="" ref={el => (image = el)}/>
          </div>
        </div>

      </div>
    </ReactScrollWheelHandler>
  )
}

export default Hello;
